# launch-pad

## Summary

Provides the ability for consumers to build standardized launch configurations for use in auto scaling groups.

## Resources

This module creates the following resources:

* [aws_iam_role](https://www.terraform.io/docs/providers/aws/r/iam_role.html)
* [aws_iam_role_policy_attachment](https://www.terraform.io/docs/providers/aws/r/iam_role_policy_attachment.html)
* [aws_security_group](https://www.terraform.io/docs/providers/aws/r/security_group.html)
* [aws_security_group_rule](https://www.terraform.io/docs/providers/aws/r/security_group_rule.html)
* [aws_iam_instance_profile](https://www.terraform.io/docs/providers/aws/r/iam_instance_profile.html)

## Cost

Using this module on its own doesn't cost anything.

## Inputs

| Name                 | Description                                                                                                                             | Type          | Default | Required |
|:---------------------|:----------------------------------------------------------------------------------------------------------------------------------------|:--------------|:--------|:---------|
| application          | Name of the application the resource(s) will be a part of.                                                                              | `string`      | -       | yes      |
| environment          | Name of the environment the resource(s) will be a part of. Used to lookup the VPC by name.                                              | `string`      | -       | yes      |
| role                 | Name of the role the resource(s) will be performing.                                                                                    | `string`      | -       | yes      |
| iam_role_description | A description for the IAM role.                                                                                                         | `string`      | -       | no       |
| iam_role_tags        | Additional tags to attach to the IAM role.                                                                                              | `map(string)` | `{}`    | no       |
| assume_role_policy   | The assume role IAM policy thats attached to the IAM role. If not set, the default EC2 assume role policy will be used.                 | `string`      | -       | no       |
| enable_admin_access  | Includes the `admin-access` security group in the list of returned groups; enabling opening ports for SSH / RDP from the admin bastion. | `bool`        | `true`  | no       |
| allow_all_egress     | Whether or not to allow all egress traffic in the security group.                                                                       | `bool`        | `true`  | no       |
| tags                 | Additional tags to attach to all resources created by this module.                                                                      | `map(string)` | `{}`    | no       |

## Outputs

| Name                        | Description                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|:----------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| name                        | The name for the launch configuration. This output should be used in cases where a service will be upgraded in-place where only one launch configuration exists at any given time. This means updates to a launch configuration that force recreation will create an outage of the service. If `name` is set in the launch configuration, `name_prefix` cannot be used.                                                                      |
| name_prefix                 | The name prefix for the launch configuration. This output should be used in cases where a service can be upgraded in a rolling or blue-green deployment strategy. Using a `name_prefix` in a launch configuration allows for multiple versions to exist at the same time; easing the transition between the old and new version of AMIs / launch configurations. If `name_prefix` is set in the launch configuration, `name` cannot be used. |
| security_group_ids          | The security groups that should be used to build the launch configuration. If enabled, a group allowing RDP / SSH access from the admin bastion will be included.                                                                                                                                                                                                                                                                            |
| vpc_id                      | The ID of the VPC.                                                                                                                                                                                                                                                                                                                                                                                                                           |
| zone_id                     | The ID of the Route53 zone.                                                                                                                                                                                                                                                                                                                                                                                                                  |
| primary_security_group_id   | The ID of the primary security group that should be used to allow service-specific network traffic. Add your service-specific traffic rules to this group.                                                                                                                                                                                                                                                                                   |
| primary_security_group_arn  | The ARN of the primary security group.                                                                                                                                                                                                                                                                                                                                                                                                       |
| primary_security_group_name | The name of the primary security group.                                                                                                                                                                                                                                                                                                                                                                                                      |
| iam_role_id                 | The ID of the IAM role.                                                                                                                                                                                                                                                                                                                                                                                                                      |
| iam_role_arn                | The ARN of the IAM role.                                                                                                                                                                                                                                                                                                                                                                                                                     |
| iam_role_name               | The name of the IAM role.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| iam_instance_profile_id     | The ID of the IAM instance profile.                                                                                                                                                                                                                                                                                                                                                                                                          |
| iam_instance_profile_arn    | The ARN of the IAM instance profile.                                                                                                                                                                                                                                                                                                                                                                                                         |
| iam_instance_profile_name   | The name of the IAM instance profile.                                                                                                                                                                                                                                                                                                                                                                                                        |
| tags                        | A combined list of tags including input tags and tags created by this module.                                                                                                                                                                                                                                                                                                                                                                |

## Examples

### Simple Usage

```terraform
data "aws_ami" "example" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

module "launch_pad" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/launch-pad.git?ref=v1"

  application = "example_app"
  environment = "example_env"
  role        = "example_role"
}

resource "aws_security_group_rule" "allow_ssh_from_all" {
  security_group_id = module.launch_pad.primary_security_group_id

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_launch_configuration" "example" {
  image_id      = data.aws_ami.example.id
  instance_type = "t3.micro"

  name_prefix                 = module.launch_pad.name_prefix
  iam_instance_profile        = module.launch_pad.iam_instance_profile
  security_groups             = module.launch_pad.security_group_ids
}
```

## Version History

* v1 - Initial Release
