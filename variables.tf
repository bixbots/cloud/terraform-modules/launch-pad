variable "application" {
  type        = string
  description = "Name of the application the resource(s) will be a part of."
}

variable "environment" {
  type        = string
  description = "Name of the environment the resource(s) will be a part of."
}

variable "role" {
  type        = string
  description = "Name of the role the resource(s) will be performing."
}

variable "iam_role_description" {
  type        = string
  default     = ""
  description = "(Optional) A description for the IAM role."
}

variable "iam_role_tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to the IAM role."
}

variable "assume_role_policy" {
  type        = string
  default     = ""
  description = "(Optional) The assume role IAM policy thats attached to the IAM role. If not set, the default EC2 assume role policy will be used."
}

variable "enable_admin_access" {
  type        = bool
  default     = true
  description = "(Optional) Includes the `admin-access` security group in the list of returned groups; enabling opening ports for SSH / RDP from the admin bastion."
}

variable "allow_all_egress" {
  type        = bool
  default     = true
  description = "(Optional) Whether or not to allow all egress traffic in the security group."
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "(Optional) Additional tags to attach to all resources created by this module."
}
