output "name" {
  description = <<EOF
    The name for the launch configuration. This output should be used in cases where a service will be upgraded in-place
    where only one launch configuration exists at any given time. This means updates to a launch configuration that
    force recreation will create an outage of the service.

    If `name` is set in the launch configuration, `name_prefix` cannot be used.
EOF

  value = local.common_name
}

output "name_prefix" {
  description = <<EOF
    The name prefix for the launch configuration. This output should be used in cases where a service can be upgraded
    in a rolling or blue-green deployment strategy. Using a `name_prefix` in a launch configuration allows for multiple
    versions to exist at the same time; easing the transition between the old and new version of AMIs / launch configurations.

    If `name_prefix` is set in the launch configuration, `name` cannot be used.
EOF

  value = "${local.common_name}-"
}

output "security_group_ids" {
  description = <<EOF
    The security groups that should be used to build the launch configuration.
    If enabled, a group allowing RDP / SSH access from the admin bastion will be included.
EOF

  value = concat([aws_security_group.primary.id], data.aws_security_group.admin.*.id)
}

# vpc
output "vpc_id" {
  value = var.vpc_id
}

# security_group
output "primary_security_group_id" {
  description = "The ID of the primary security group that should be used to allow service-specific network traffic. Add your service-specific traffic rules to this group."
  value       = aws_security_group.primary.id
}
output "primary_security_group_arn" {
  description = "The ARN of the primary security group."
  value       = aws_security_group.primary.arn
}
output "primary_security_group_name" {
  description = "The name of the primary security group."
  value       = aws_security_group.primary.name
}

// iam_role
output "iam_role_id" {
  description = "The ID of the IAM role."
  value       = aws_iam_role.primary.id
}
output "iam_role_arn" {
  description = "The ARN of the IAM role."
  value       = aws_iam_role.primary.arn
}
output "iam_role_name" {
  description = "The name of the IAM role."
  value       = aws_iam_role.primary.name
}

// iam_instance_profile
output "iam_instance_profile_id" {
  description = "The ID of the IAM instance profile."
  value       = aws_iam_instance_profile.primary.id
}
output "iam_instance_profile_arn" {
  description = "The ARN of the IAM instance profile."
  value       = aws_iam_instance_profile.primary.arn
}
output "iam_instance_profile_name" {
  description = "The name of the IAM instance profile."
  value       = aws_iam_instance_profile.primary.name
}

# tags
output "tags" {
  description = "A combined list of tags including input tags and tags created by this module."
  value       = local.common_tags
}
