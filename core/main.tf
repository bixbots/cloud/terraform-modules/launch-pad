locals {
  common_name = "${var.application}-${var.environment}-${var.role}"
  common_tags = merge(var.tags, {
    "Role" = var.role
  })
}

data "aws_iam_policy_document" "assume_role_ec2" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "primary" {
  name               = local.common_name
  description        = var.iam_role_description
  assume_role_policy = coalesce(var.assume_role_policy, data.aws_iam_policy_document.assume_role_ec2.json)

  tags = merge(local.common_tags, var.iam_role_tags, {
    "Name" = local.common_name
  })
}

resource "aws_iam_role_policy_attachment" "ssm_core" {
  role       = aws_iam_role.primary.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

data "aws_security_group" "admin" {
  count = var.enable_admin_access ? 1 : 0

  vpc_id = var.vpc_id
  name   = "${var.environment}-admin-access"
}

resource "aws_security_group" "primary" {
  vpc_id = var.vpc_id
  name   = local.common_name

  tags = merge(local.common_tags, {
    "Name" = local.common_name
  })
}

resource "aws_security_group_rule" "allow_all_egress" {
  count = var.allow_all_egress ? 1 : 0

  security_group_id = aws_security_group.primary.id

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "ALL"
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_iam_instance_profile" "primary" {
  name = aws_iam_role.primary.name
  role = aws_iam_role.primary.name
}
