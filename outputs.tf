output "name" {
  description = <<EOF
    The name for the launch configuration. This output should be used in cases where a service will be upgraded in-place
    where only one launch configuration exists at any given time. This means updates to a launch configuration that
    force recreation will create an outage of the service.

    If `name` is set in the launch configuration, `name_prefix` cannot be used.
EOF

  value = module.core.name
}

output "name_prefix" {
  description = <<EOF
    The name prefix for the launch configuration. This output should be used in cases where a service can be upgraded
    in a rolling or blue-green deployment strategy. Using a `name_prefix` in a launch configuration allows for multiple
    versions to exist at the same time; easing the transition between the old and new version of AMIs / launch configurations.

    If `name_prefix` is set in the launch configuration, `name` cannot be used.
EOF

  value = module.core.name_prefix
}

output "security_group_ids" {
  description = <<EOF
    The security groups that should be used to build the launch configuration.
    If enabled, a group allowing RDP / SSH access from the admin bastion will be included.
EOF

  value = module.core.security_group_ids
}

# vpc
output "vpc_id" {
  description = "The ID of the VPC."
  value       = data.aws_vpc.primary.id
}

# zone
output "zone_id" {
  description = "The ID of the Route53 zone."
  value       = data.aws_route53_zone.primary.zone_id
}

# security_group
output "primary_security_group_id" {
  description = "The ID of the primary security group that should be used to allow service-specific network traffic. Add your service-specific traffic rules to this group."
  value       = module.core.primary_security_group_id
}
output "primary_security_group_arn" {
  description = "The ARN of the primary security group."
  value       = module.core.primary_security_group_arn
}
output "primary_security_group_name" {
  description = "The name of the primary security group."
  value       = module.core.primary_security_group_name
}

// iam_role
output "iam_role_id" {
  description = "The ID of the IAM role."
  value       = module.core.iam_role_id
}
output "iam_role_arn" {
  description = "The ARN of the IAM role."
  value       = module.core.iam_role_arn
}
output "iam_role_name" {
  description = "The name of the IAM role."
  value       = module.core.iam_role_name
}

// iam_instance_profile
output "iam_instance_profile_id" {
  description = "The ID of the IAM instance profile."
  value       = module.core.iam_instance_profile_id
}
output "iam_instance_profile_arn" {
  description = "The ARN of the IAM instance profile."
  value       = module.core.iam_instance_profile_arn
}
output "iam_instance_profile_name" {
  description = "The name of the IAM instance profile."
  value       = module.core.iam_instance_profile_name
}

# tags
output "tags" {
  description = "A combined list of tags including input tags and tags created by this module."
  value       = module.core.tags
}
