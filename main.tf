data "aws_vpc" "primary" {
  tags = {
    "Name" = var.environment
  }
}

data "aws_route53_zone" "primary" {
  name = data.aws_vpc.primary.tags["DNSZone"]
}

module "core" {
  source = "./core"

  application          = var.application
  environment          = var.environment
  role                 = var.role
  vpc_id               = data.aws_vpc.primary.id
  iam_role_description = var.iam_role_description
  iam_role_tags        = var.iam_role_tags
  assume_role_policy   = var.assume_role_policy
  enable_admin_access  = var.enable_admin_access
  allow_all_egress     = var.allow_all_egress
  tags                 = var.tags
}
